@extends('layouts.app')

@section('content')


		
		<div class="row">
			<div class="col-md-12">
				<!-- TABLE STRIPED -->
				<div class="panel">
					<div class="panel-heading">
						<!-- <h3 class="panel-title">@include('admin._caminho')</h3>-->
						<a title="Novo Processo" class="btn btn-success" href="{{ route('processos.create')}}"><i class="material-icons">Novo Processo</i></a>
					</div>
					<div class="panel-body">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>#</th>
									<th>Número Processo</th>
									<th>Assunto</th>
									<th>Caixa</th>
								</tr>
							</thead>
							<tbody>
							@foreach($registros as $registro)
							<tr>
								<td>{{ $registro->id }}</td>
								<td>{{ $registro->titulo }}</td>
								<td>{{ $registro->descricao }}</td>
								<td>

									<form action="" method="post">
										@can('carros-edit')
										<a title="Editar" class="btn orange" href=""><i class="material-icons">mode_edit</i></a>
										<a title="Galeria" class="btn blue" href=""><i class="material-icons">image</i></a>

										@endcan
										@can('carros-delete')
											{{ method_field('DELETE') }}
											{{ csrf_field() }}
											<button title="Deletar" class="btn red"><i class="material-icons">delete</i></button>
										@endcan
									</form>

								</td>
							</tr>
							@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

@endsection
