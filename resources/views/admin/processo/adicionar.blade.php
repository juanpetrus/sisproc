@extends('layouts.app')

@section('content')
	<div class="row">
	<div class="col-md-12">
				<!-- TABLE STRIPED -->
				<div class="panel">
					<div class="panel-body">

						<form action="{{ route('processos.store') }}" method="post">
						{{csrf_field()}}
						@include('admin.processo._form')
						<div class="form-group col-md-2">
						<button class="btn btn-success">Adicionar</button>
						</div>
						</form>

					</div>
				</div>
	</div>
	</div>
@endsection