@extends('layouts.app')

@section('content')
<!-- OVERVIEW -->
                    
<div class="panel panel-headline">
						<div class="panel-heading">
							<h3 class="panel-title">Seja Bem Vindo</h3>
							<p class="panel-subtitle">Sistema Integrado de Processos CMR</p>
						</div>						
					</div>
					<!-- END OVERVIEW -->
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
