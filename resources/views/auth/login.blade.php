@extends('layouts.log')

@section('content')
<div class="vertical-align-wrap">
			<div class="vertical-align-middle">
				<div class="auth-box ">
					<div class="left">
						<div class="content">
							<div class="header">
								<div class="logo text-center"><img src="assets/img/logo-sisproc.png" width="300" alt="Sisproc Logo"></div>
								<p class="lead">Faça login com sua conta</p>
							</div>
                            <form class="form-auth-small" method="POST" action="{{ route('login') }}">
                                @csrf
								<div class="form-group">
									<label for="email" class="control-label sr-only">{{ __('E-Mail Address') }}</label>
                                    <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
                                    @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
								</div>
								<div class="form-group">
									<label for="signin-password" class="control-label sr-only">{{ __('Password') }}</label>
									<input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" name="password" value="thisisthepassword" placeholder="Password" required>
                                    @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
								<div class="form-group clearfix">
									<label class="fancy-checkbox element-left">
										<input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
										<span>Lembrar-me</span>
									</label>
								</div>
								<button type="submit" class="btn btn-primary btn-lg btn-block">LOGIN</button>
								<div class="bottom">
                                    <span class="helper-text"><i class="fa fa-lock"></i> 
                                    @if (Route::has('password.request'))
                                    <a href="{{ route('password.request') }}">{{ __('Esqueceu sua Senha?') }}</a>
                                    @endif
                                    </span>                                
								</div>
							</form>
						</div>
					</div>
					<div class="right">
						<div class="overlay"></div>
						<div class="content text">
							<h1 class="heading">Sistema Integrado de Processos</h1>
							<p>by Juan Petrus</p>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
@endsection
