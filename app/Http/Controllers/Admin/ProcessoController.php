<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Gate;

use App\Processo;

class ProcessoController extends Controller
{
    public function index()
    {
        $registros = Processo::orderBy('id','DESC')->paginate(5);
        $caminhos = [
            ['url'=>'','titulo'=>'Admin']
        ];
        return view('admin.processo.index',compact('caminhos', 'registros'));
    }

    public function create()
    {
   

      $caminhos = [
      ['url'=>'/admin','titulo'=>'Admin'],
      ['url'=>route('processos.index'),'titulo'=>'Processos'],
      ['url'=>'','titulo'=>'Adicionar']
      ];

      return view('admin.processo.adicionar',compact('caminhos','marcas','categorias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
            'titulo' => 'required',
            'descricao' => 'required',
            'valor' => 'required|numeric',
            'ano' => 'required|numeric',
            'marca_id' => 'required|numeric',
      ]);

      $dados = $request->all();

      $carro = Carro::create($request->all());
      if(isset($dados['novasCategorias'])){
        foreach ($dados['novasCategorias'] as $key => $value) {
          $carro->categorias()->save(Categoria::find($value));
        }
      }

      return redirect()->route('carros.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Carro $carro)
    {
      if(Gate::denies('carros-edit')){
        abort(403,"Não autorizado!");
      }

      $registro = $carro;

      $marcas = Marca::all();
      $categorias = Categoria::all();

      $caminhos = [
      ['url'=>'/admin','titulo'=>'Admin'],
      ['url'=>route('carros.index'),'titulo'=>'Carros'],
      ['url'=>'','titulo'=>'Editar']
      ];

      return view('admin.carro.editar',compact('registro','caminhos','marcas','categorias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Carro $carro)
    {
      if(Gate::denies('carros-edit')){
        abort(403,"Não autorizado!");
      }

      $this->validate($request, [
            'titulo' => 'required',
            'descricao' => 'required',
            'valor' => 'required|numeric',
            'ano' => 'required|numeric',
            'marca_id' => 'required|numeric',
      ]);

      $dados = $request->all();
      $registro = $carro;

      $registro->update($request->all());
      foreach ($registro->categorias as $key => $value) {
        $registro->categorias()->detach($value);
      }
      if(isset($dados['novasCategorias'])){
        foreach ($dados['novasCategorias'] as $key => $value) {
          $registro->categorias()->save(Categoria::find($value));
        }
      }

      return redirect()->route('carros.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Carro $carro)
    {
      if(Gate::denies('carros-delete')){
        abort(403,"Não autorizado!");
      }

      foreach ($carro->categorias as $key => $value) {
        $carro->categorias()->detach($value);
      }

      foreach ($carro->imagens as $key => $value) {
        $value->delete();
      }

      $carro->delete();
      return redirect()->route('carros.index');
    }

}
