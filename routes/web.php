<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::group(['middleware' => 'auth','prefix' => 'admin'], function () {
    
    Route::get('/', 'Admin\AdminController@index');
    Route::resource('usuarios', 'Admin\UsuarioController');
  
    Route::get('usuarios/papel/{id}', ['as'=>'usuarios.papel','uses'=>'Admin\UsuarioController@papel']);
    Route::post('usuarios/papel/{papel}', ['as'=>'usuarios.papel.store','uses'=>'Admin\UsuarioController@papelStore']);
    Route::delete('usuarios/papel/{usuario}/{papel}', ['as'=>'usuarios.papel.destroy','uses'=>'Admin\UsuarioController@papelDestroy']);
  
    Route::resource('papeis', 'Admin\PapelController');
  
    Route::get('papeis/permissao/{id}', ['as'=>'papeis.permissao','uses'=>'Admin\PapelController@permissao']);
    Route::post('papeis/permissao/{permissao}', ['as'=>'papeis.permissao.store','uses'=>'Admin\PapelController@permissaoStore']);
    Route::delete('papeis/permissao/{papel}/{permissao}', ['as'=>'papeis.permissao.destroy','uses'=>'Admin\PapelController@permissaoDestroy']);
  
    Route::get('imagens/excluidas', ['as'=>'imagens.excluidas','uses'=>'Admin\ImagemController@excluidas']);
    Route::put('imagens/recupera/{id}', ['as'=>'imagens.recupera','uses'=>'Admin\ImagemController@recupera']);
  
    Route::resource('imagens', 'Admin\ImagemController');
    
    Route::resource('processos', 'Admin\ProcessoController');

  
    Route::get('perfil', ['as'=>'site.perfil','uses'=>'Site\SiteController@perfil']);
    Route::put('perfil', ['as'=>'site.perfil.update','uses'=>'Site\SiteController@perfilUpdate']);
  
    Route::get('favoritos', ['as'=>'site.favoritos','uses'=>'Site\SiteController@favoritos']);
  
    Route::post('favoritos/{carro}', ['as'=>'site.favoritos.create','uses'=>'Site\SiteController@favoritosCreate']);
    Route::delete('favoritos/{carro}', ['as'=>'site.favoritos.delete','uses'=>'Site\SiteController@favoritosDelete']);
  });
  